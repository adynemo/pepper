<?php

namespace ady\pepper\constant;

class team_status
{
	public const FULL = 1;
	public const MISSING = 2;
	public const EMPTY = 3;

	public const KEYS = [
		self::FULL    => 'full',
		self::MISSING => 'missing',
		self::EMPTY   => 'empty',
	];
}
