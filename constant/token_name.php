<?php

namespace ady\pepper\constant;

class token_name
{
	public const SEARCH_USERS = 'search-users';
	public const CREATE_PROJECT = 'create-project';
	public const DELETE_PROJECT = 'delete-project';
}
