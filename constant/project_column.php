<?php

namespace ady\pepper\constant;

class project_column
{
	public const ID = 'id';
	public const TITLE = 'title';
	public const URL = 'url';
	public const TRANSLATORS = 'translators';
	public const EDITORS = 'editors';
	public const FIRST_CHECKERS = 'first_checkers';
	public const LAST_CHECKERS = 'last_checkers';
}
