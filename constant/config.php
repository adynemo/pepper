<?php

/**
 * @package   phpBB Extension - DC-Trad Pepper
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\pepper\constant;

class config
{
	public const TABLE_NAME = 'ady_pepper';
	public const PERMISSION_ORGANIZER = 'u_ady_pepper_organizer';
}
