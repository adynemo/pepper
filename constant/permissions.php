<?php

/**
 * @package   phpBB Extension - DC-Trad Pepper
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\pepper\constant;

class permissions
{
	public const U_ORGANIZER = 'u_ady_pepper_organizer';
}
