<?php

/**
 * @package   phpBB Extension - DC-Trad Pepper
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\pepper\controller;

use ady\pepper\constant\permissions;
use ady\pepper\constant\token_name;
use ady\pepper\core\repository;
use phpbb\auth\auth;
use phpbb\config\config;
use phpbb\controller\helper;
use phpbb\exception\http_exception;
use phpbb\log\log;
use phpbb\request\request;
use phpbb\template\template;
use phpbb\user;
use Symfony\Component\HttpFoundation\Response;

class organizer
{
	private user $user;
	private request $request;
	private log $log;
	private repository $repository;
	private auth $auth;
	private helper $helper;
	private template $template;
	private config $config;

	public function __construct(
		user $user,
		request $request,
		log $log,
		repository $repository,
		auth $auth,
		helper $helper,
		template $template,
		config $config
	)
	{
		$this->user = $user;
		$this->request = $request;
		$this->log = $log;
		$this->repository = $repository;
		$this->auth = $auth;
		$this->helper = $helper;
		$this->template = $template;
		$this->config = $config;
	}

	public function organize(): Response
	{
		$this->check_permission(permissions::U_ORGANIZER);
		try
		{
			$projects = $this->repository->find_all_projects();
		}
		catch (\Throwable $exception)
		{
			$this->log->add(
				'critical',
				$this->user->data['user_id'],
				$this->user->ip,
				'LOG_PEPPER_FETCH_PROJECTS_ERROR',
				false,
				[(string) $exception]
			);
			trigger_error("Unable to fetch projects:\n" . $exception->getMessage(), E_USER_ERROR);
		}

		usort($projects, function ($a, $b) {
			if ($a->status === $b->status)
			{
				return $a->name > $b->name;
			}

			return ($a->status > $b->status) ? 1 : -1;
		});

		$this->template->assign_vars([
			'projects'           => $projects,
			'search_users_api'   => $this->helper->route('ady_pepper_api_search_users'),
			'create_project_api' => $this->helper->route('ady_pepper_api_upsert_project'),
			'delete_project_api' => $this->helper->route('ady_pepper_api_delete_project'),
			'api_tokens'         => $this->create_tokens(),
			'on_ady_pepper'      => true,
		]);

		return $this->helper->render('organizer.html', 'Pepper');
	}

	private function check_permission(string $permission): void
	{
		if (!$this->auth->acl_get($permission))
		{
			if (!$this->user->data['is_registered'])
			{
				$user = $this->request->server('PHP_AUTH_USER');
				$pw = $this->request->server('PHP_AUTH_PW');
				if (!empty($user) && !empty($pw))
				{
					$this->auth->login($user, $pw);
				}
			}

			throw new http_exception(Response::HTTP_FORBIDDEN, 'NOT_AUTHORISED');
		}
	}

	public function create_tokens(): string
	{
		$now = (new \DateTimeImmutable())->getTimestamp();
		$token_sid = ($this->user->data['user_id'] == ANONYMOUS && !empty($this->config['form_token_sid_guests'])) ? $this->user->session_id : '';
		$search_token = sha1($now . $this->user->data['user_form_salt'] . token_name::SEARCH_USERS . $token_sid);
		$create_token = sha1($now . $this->user->data['user_form_salt'] . token_name::CREATE_PROJECT . $token_sid);
		$delete_token = sha1($now . $this->user->data['user_form_salt'] . token_name::DELETE_PROJECT . $token_sid);

		return json_encode([
			'creation_time' => $now,
			'search_token'  => $search_token,
			'create_token'  => $create_token,
			'delete_token'  => $delete_token,
		]);
	}
}
