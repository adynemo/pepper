<?php

/**
 * @package   phpBB Extension - DC-Trad Pepper
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\pepper\controller;

use ady\pepper\constant\http;
use ady\pepper\constant\permissions;
use ady\pepper\constant\token_name;
use ady\pepper\core\repository;
use phpbb\auth\auth;
use phpbb\log\log;
use phpbb\request\request;
use phpbb\user;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class api
{
	private user $user;
	private request $request;
	private log $log;
	private repository $repository;
	private auth $auth;

	public function __construct(
		user $user,
		request $request,
		log $log,
		repository $repository,
		auth $auth
	)
	{
		$this->user = $user;
		$this->request = $request;
		$this->log = $log;
		$this->repository = $repository;
		$this->auth = $auth;
	}

	public function search_users(): Response
	{
		$response = $this->allow_request(token_name::SEARCH_USERS);
		if (null !== $response)
		{
			return $response;
		}

		$query = $this->request->variable('q', '', true);

		if ('' === $query)
		{
			return new JsonResponse([]);
		}

		try
		{
			$users = $this->repository->find_users_by_query(strtolower($query));
		}
		catch (\Throwable $exception)
		{
			$this->log->add(
				'critical',
				$this->user->data['user_id'],
				$this->user->ip,
				'LOG_PEPPER_FETCH_USERS_ERROR',
				false,
				[$query, (string) $exception]
			);
			return new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
		}

		$light_users = [];
		foreach ($users as $user)
		{
			$light_users[] = [
				'id'       => $user['user_id'],
				'avatar'   => $user['avatar'],
				'username' => $user['username'],
			];
		}

		return new JsonResponse($light_users);
	}

	public function upsert_project(): JsonResponse
	{
		$response = $this->allow_request(token_name::CREATE_PROJECT);
		if (null !== $response)
		{
			return $response;
		}

		$id = $this->request->variable('id', 0);
		$title = $this->request->variable('title', '', true);
		$url = $this->request->variable('url', '', true);
		$translators = $this->request->variable('translators', '');
		$first_checkers = $this->request->variable('first_checkers', '');
		$editors = $this->request->variable('editors', '');
		$last_checkers = $this->request->variable('last_checkers', '');

		$this->log->add(
			'admin',
			$this->user->data['user_id'],
			$this->user->ip,
			'LOG_PEPPER_UPSERT_PROJECT',
			false,
			[json_encode([
				$id,
				$title,
				$url,
				$translators,
				$first_checkers,
				$editors,
				$last_checkers,
			])]
		);

		$translators = $this->sanitize_job($translators);
		$first_checkers = $this->sanitize_job($first_checkers);
		$editors = $this->sanitize_job($editors);
		$last_checkers = $this->sanitize_job($last_checkers);

		$exception = null;
		try
		{
			$success = $this->repository->upsert_project($id, $title, $url, $translators, $first_checkers, $editors, $last_checkers);
		}
		catch (\Throwable $exception)
		{
			$success = false;
		}

		if (!$success)
		{
			$this->log->add(
				'critical',
				$this->user->data['user_id'],
				$this->user->ip,
				'LOG_PEPPER_UPSERT_PROJECT_ERROR',
				false,
				[json_encode([
					$id,
					$title,
					$url,
					$translators,
					$first_checkers,
					$editors,
					$last_checkers,
				]), (string) $exception]
			);
			return new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
		}

		return new JsonResponse(null, Response::HTTP_ACCEPTED);
	}

	public function delete_project(int $id): JsonResponse
	{
		$response = $this->allow_request(token_name::DELETE_PROJECT);
		if (null !== $response)
		{
			return $response;
		}

		if (0 >= $id)
		{
			return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
		}

		$this->log->add(
			'admin',
			$this->user->data['user_id'],
			$this->user->ip,
			'LOG_PEPPER_DELETE_PROJECT',
			false,
			[$id]
		);

		$exception = null;
		try
		{
			$success = $this->repository->delete_project($id);
		}
		catch (\Throwable $exception)
		{
			$success = false;
		}

		if (!$success)
		{
			$this->log->add(
				'critical',
				$this->user->data['user_id'],
				$this->user->ip,
				'LOG_PEPPER_DELETE_PROJECT_ERROR',
				false,
				[$id, (string) $exception]
			);
			return new JsonResponse(null, Response::HTTP_INTERNAL_SERVER_ERROR);
		}

		return new JsonResponse(null, Response::HTTP_ACCEPTED);
	}

	private function allow_request(string $token): ?JsonResponse
	{
		if (!$this->user->data['is_registered'])
		{
			return new JsonResponse(null, Response::HTTP_UNAUTHORIZED);
		}
		if (!$this->auth->acl_get(permissions::U_ORGANIZER))
		{
			return new JsonResponse(null, Response::HTTP_FORBIDDEN);
		}
		if (!$this->request->is_ajax())
		{
			return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
		}
		if (http::POST !== $this->request->server('REQUEST_METHOD'))
		{
			return new JsonResponse(null, Response::HTTP_METHOD_NOT_ALLOWED);
		}
		if (false === check_form_key($token))
		{
			return new JsonResponse(null, Response::HTTP_BAD_REQUEST);
		}

		return null;
	}

	private function sanitize_job($job): array
	{
		$san_job = [];

		if (empty($job) || !is_string($job))
		{
			return $san_job;
		}

		$job = explode(',', $job);
		foreach ($job as $id)
		{
			$san_id = filter_var($id, FILTER_VALIDATE_INT);
			if (false !== $san_id)
			{
				$san_job[] = $san_id;
			}
		}

		return $san_job;
	}
}
