<?php

/**
 * @package   phpBB Extension - DC-Trad Pepper
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\pepper\migrations;

use ady\pepper\constant\config;
use ady\pepper\constant\permissions;
use ady\pepper\constant\project_column;

class pepper_1_0_0 extends \phpbb\db\migration\migration
{
	public function update_data()
	{
		return [
			['module.add', [
				'acp',
				'ACP_CAT_DOT_MODS',
				'ACP_CAT_PEPPER',
			]],
			['module.add', [
				'acp',
				'ACP_CAT_PEPPER',
				[
					'module_basename' => '\ady\pepper\acp\acp_pepper_module',
					'modes'           => ['settings'],
				],
			]],
			['permission.add', [permissions::U_ORGANIZER]],
		];
	}

	public function update_schema()
	{
		return [
			'add_tables' => [
				$this->table_prefix . config::TABLE_NAME => [
					'COLUMNS'     => [
						project_column::ID             => ['UINT', null, 'auto_increment'],
						project_column::TITLE          => ['VCHAR', ''],
						project_column::TRANSLATORS    => ['VCHAR', ''],
						project_column::EDITORS        => ['VCHAR', ''],
						project_column::FIRST_CHECKERS => ['VCHAR', ''],
						project_column::LAST_CHECKERS  => ['VCHAR', ''],
						project_column::URL            => ['VCHAR', ''],
					],
					'PRIMARY_KEY' => 'id',
				],
			],
		];
	}

	public function revert_schema()
	{
		return [
			'drop_tables' => [
				$this->table_prefix . config::TABLE_NAME,
			],
		];
	}
}
