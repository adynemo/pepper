<?php

/**
 * @package   phpBB Extension - DC-Trad Pepper
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\pepper\migrations;

class pepper_1_0_2 extends \phpbb\db\migration\migration
{
	public function update_data()
	{
		return [
			['module.remove', [
				'acp',
				'ACP_CAT_PEPPER',
				[
					'module_basename' => '\ady\pepper\acp\acp_pepper_module',
					'modes'           => ['settings'],
				],
			]],
			['module.remove', [
				'acp',
				'ACP_CAT_DOT_MODS',
				'ACP_CAT_PEPPER',
			]],
		];
	}
}
