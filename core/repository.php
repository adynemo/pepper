<?php

/**
 * @package   phpBB Extension - DC-Trad Pepper
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\pepper\core;

use ady\pepper\constant\config;
use ady\pepper\constant\project_column;
use ady\pepper\entity\project;
use phpbb\db\driver\driver_interface;
use phpbb\path_helper;
use phpbb\user;

class repository
{
	private driver_interface $db;
	private string $projects_table_name;
	private string $user_table_name;
	private user $user;
	private path_helper $path_helper;

	public function __construct(driver_interface $db, user $user, path_helper $path_helper, string $table_prefix)
	{
		$this->db = $db;
		$this->user = $user;
		$this->path_helper = $path_helper;
		$this->projects_table_name = $table_prefix . config::TABLE_NAME;
		$this->user_table_name = USERS_TABLE;
	}

	public function find_all_projects(): array
	{
		$raw_projects = [];

		$sql = 'SELECT * FROM ' . $this->projects_table_name . ' ORDER BY ' . project_column::TITLE . ' ASC';
		$result = $this->db->sql_query($sql);

		if (!$result)
		{
			$this->db->sql_freeresult($result);
			return $raw_projects;
		}

		$users_id = [];
		while ($row = $this->db->sql_fetchrow($result))
		{
			$raw_projects[] = $row;
			$ids = $this->get_users_id_from_projects($row);
			if ([] !== $ids)
			{
				$users_id[] = $ids;
			}
		}
		$this->db->sql_freeresult($result);

		if ([] === $raw_projects)
		{
			return $raw_projects;
		}

		$users_id = array_unique(array_merge(...$users_id));
		$users = $this->find_users_by_id($users_id);

		$projects = [];
		foreach ($raw_projects as $raw_project)
		{
			$count = 0;
			$project = new project();
			$project
				->set_id($raw_project[project_column::ID])
				->set_name($raw_project[project_column::TITLE])
				->set_url($raw_project[project_column::URL])
			;
			$users_id = $this->get_array_id($raw_project[project_column::TRANSLATORS]);
			foreach ($users_id as $id)
			{
				$this->increase_user_count($count, $id);
				$project->add_translator($users[$id]);
			}
			$users_id = $this->get_array_id($raw_project[project_column::EDITORS]);
			foreach ($users_id as $id)
			{
				$this->increase_user_count($count, $id);
				$project->add_editor($users[$id]);
			}
			$users_id = $this->get_array_id($raw_project[project_column::FIRST_CHECKERS]);
			foreach ($users_id as $id)
			{
				$this->increase_user_count($count, $id);
				$project->add_first_checker($users[$id]);
			}
			$users_id = $this->get_array_id($raw_project[project_column::LAST_CHECKERS]);
			foreach ($users_id as $id)
			{
				$this->increase_user_count($count, $id);
				$project->add_last_checker($users[$id]);
			}

			$project
				->set_status()
				->set_count($count)
			;

			$projects[] = $project;
		}

		return $projects;
	}

	public function find_users_by_id(array $users_id): array
	{
		$users = [];
		if ([] === $users_id)
		{
			return $users;
		}

		$users_id = implode(',', $users_id);

		$sql = 'SELECT * FROM ' . $this->user_table_name . ' WHERE user_id IN (' . $users_id . ')';
		$result = $this->db->sql_query($sql);

		if (!$result)
		{
			$this->db->sql_freeresult($result);
			return $users;
		}

		while ($row = $this->db->sql_fetchrow($result))
		{
			$row['avatar'] = $this->avatar_img_resize($row);
			$users[$row['user_id']] = $row;
		}
		$this->db->sql_freeresult($result);

		return $users;
	}

	public function find_users_by_query(string $query): array
	{
		$users = [];
		$user_table = $this->user_table_name;
		$user_group_table = USER_GROUP_TABLE;
		$sql = <<<EOT
SELECT * FROM $user_table AS u
INNER JOIN $user_group_table AS ug ON ug.user_id = u.user_id
WHERE u.username_clean LIKE "%$query%"
AND ug.group_id = 8
ORDER BY u.username ASC
EOT;

		$result = $this->db->sql_query($sql);

		if (!$result)
		{
			$this->db->sql_freeresult($result);
			return $users;
		}

		while ($row = $this->db->sql_fetchrow($result))
		{
			$row['avatar'] = $this->avatar_img_resize($row);
			$users[$row['user_id']] = $row;
		}
		$this->db->sql_freeresult($result);

		return $users;
	}

	public function upsert_project(
		int $id,
		string $title,
		string $url,
		array $translators,
		array $first_checkers,
		array $editors,
		array $last_checkers
	): bool
	{
		$project = [
			project_column::TITLE          => $title,
			project_column::URL            => $url,
			project_column::TRANSLATORS    => implode(',', $translators),
			project_column::EDITORS        => implode(',', $editors),
			project_column::FIRST_CHECKERS => implode(',', $first_checkers),
			project_column::LAST_CHECKERS  => implode(',', $last_checkers),
		];

		if (0 === $id)
		{
			$sql = 'INSERT INTO ' . $this->projects_table_name . ' ' . $this->db->sql_build_array('INSERT', $project);
		}
		else
		{
			$sql = 'UPDATE ' . $this->projects_table_name . '
			SET ' . $this->db->sql_build_array('UPDATE', $project) . '
    		WHERE id = ' . $id;
		}

		$this->db->sql_query($sql);

		return 1 === $this->db->sql_affectedrows();
	}

	public function delete_project(int $id): bool
	{
		$sql = 'DELETE FROM ' . $this->projects_table_name . ' WHERE id = ' . $id;
		$this->db->sql_query($sql);

		return 1 === $this->db->sql_affectedrows();
	}

	private function get_array_id(string $ids): array
	{
		return '' === $ids ? [] : explode(',', $ids);
	}

	private function get_users_id_from_projects(array $project): array
	{
		$translators = $this->get_array_id($project[project_column::TRANSLATORS]);
		$first_checkers = $this->get_array_id($project[project_column::FIRST_CHECKERS]);
		$editors = $this->get_array_id($project[project_column::EDITORS]);
		$last_checkers = $this->get_array_id($project[project_column::LAST_CHECKERS]);

		return array_unique([...$translators, ...$first_checkers, ...$editors, ...$last_checkers]);
	}

	private function avatar_img_resize(array $row): string
	{
		if (!empty($row['user_avatar']))
		{
			if ($row['user_avatar_width'] >= $row['user_avatar_height'])
			{
				$ratio = $row['user_avatar_width'] / $row['user_avatar_height'];
				$row['user_avatar_height'] = round(30 / $ratio);
				$row['user_avatar_width'] = 30;
			}
			else
			{
				$ratio = $row['user_avatar_height'] / $row['user_avatar_width'];
				$row['user_avatar_width'] = round(30 / $ratio);
				$row['user_avatar_height'] = 30;
			}
			return phpbb_get_user_avatar($row, '');
		}

		// Determine board url - we may need it later
		$board_url = generate_board_url() . '/';
		$corrected_path = $this->path_helper->get_web_root_path();
		$web_path = (defined('PHPBB_USE_BOARD_URL_PATH') && PHPBB_USE_BOARD_URL_PATH) ? $board_url : $corrected_path;
		$theme = "{$web_path}styles/" . rawurlencode($this->user->style['style_path']) . '/theme';

		return '<img class="avatar" src="' . $theme . '/images/no_avatar.gif" width="' . 30 . '" height="' . 30 . '" alt="">';
	}

	private function increase_user_count(int &$count, $id): void
	{

		if ($this->user->id() == $id)
		{
			$count++;
		}
	}
}
