<?php

/**
 * @package   phpBB Extension - DC-Trad Pepper
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\pepper\event;

use ady\pepper\constant\permissions;
use phpbb\event\data;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class listener implements EventSubscriberInterface
{
	static public function getSubscribedEvents()
	{
		return [
			'core.permissions' => 'permissions',
			'core.user_setup'  => 'load_language_on_setup',
		];
	}

	public function __construct()
	{
	}

	public function load_language_on_setup(data $event): void
	{
		$lang_set_ext = $event['lang_set_ext'];
		$lang_set_ext[] = [
			'ext_name' => 'ady/pepper',
			'lang_set' => 'common',
		];
		$event['lang_set_ext'] = $lang_set_ext;
	}

	public function permissions(data $event)
	{
		$permissions[permissions::U_ORGANIZER] = [
			'lang' => 'ACL_' . strtoupper(permissions::U_ORGANIZER),
			'cat'  => 'ady_dct',
		];
		$event['permissions'] = array_merge($event['permissions'], $permissions);
		$event['categories'] = array_merge($event['categories'], ['ady_dct' => 'ACP_CAT_DCT']);
	}

}
