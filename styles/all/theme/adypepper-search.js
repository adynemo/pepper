(function() {
	const elements = document.querySelectorAll('.adypepper .project-container:not(.header)');
	const searchInput = document.getElementById('search');

	if (searchInput) {
		searchInput.addEventListener('input', function(e) {
			const text = e.target.value;
			const regexp = new RegExp(text, 'i');
			const displayed = [];
			Array.prototype.forEach.call(elements, function(element) {
				const title = element.querySelector('.name').innerText;
				const users = element.querySelectorAll('.user.chip');
				let display = 'none';
				for (const user of users) {
					if (user.innerText.match(regexp)) {
						display = 'flex';
						break;
					}
				}
				if ('none' === display && title.match(regexp)) {
					display = 'flex';
				}
				element.style.display = display;
				if ('flex' === display) {
					displayed.push(element);
				}
			});
			Array.prototype.forEach.call(displayed, function(element, i) {
				if (i % 2) {
					element.classList.add('odd');
				} else {
					element.classList.remove('odd');
				}
			});
		});
	}

	const legends = document.querySelectorAll('.adypepper label[for="search"] button.sort');
	let currentFilter = '';
	for (const legend of legends) {
		legend.addEventListener('click', function(e) {
			const button = e.target;
			const filter = button.dataset.filter;
			if (currentFilter === filter) {
				currentFilter = '';
				button.classList.remove('active');
				Array.prototype.forEach.call(elements, function(element) {
					element.style.display = 'flex';
				});
			} else {
				currentFilter = filter;
				Array.prototype.forEach.call(legends, function(legend) {
					legend.classList.remove('active');
				});
				button.classList.add('active');
				Array.prototype.forEach.call(elements, function(element) {
					element.style.display = element.classList.contains('team-' + filter) ? 'flex' : 'none';
				});
			}
		})
	}
})()
