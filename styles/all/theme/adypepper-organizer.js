(function() {
	const projectsContainer = document.getElementById('projects-container');
	const form = document.getElementById('creation');
	const fieldsContainer = document.getElementById('fields-container');
	const formTitle = document.getElementById('toggle-form');
	const searchEndpoint = form.dataset.adyPepperSearchApi;
	const createEndpoint = form.dataset.adyPepperCreateApi;
	const deleteEndpoint = form.dataset.adyPepperDeleteApi;
	const i18n = {
		generalError: form.dataset.adyPepperError,
		removalConfirmation: form.dataset.adyPepperRemovalConfirmation,
		chipTitle: form.dataset.adyPepperChipTitle,
	};
	const apiTokens = JSON.parse(form.dataset.adyPepperApiTokens);
	const errorMessage = document.getElementById('error-message');
	const infoMessage = document.getElementById('info-message');
	form.addEventListener('submit', submitForm);
	form.querySelector('input.reset').addEventListener('click', resetForm);

	formTitle.addEventListener('click', function() {
		resetForm();
		toggleForm();
	});

	const projectContainers = projectsContainer.querySelectorAll('.project-container:not(.header)');
	let totalProjects = 0;
	let projectsCount = 0;
	let jobsCount = 0;
	for (const container of projectContainers) {
		totalProjects++;
		const currentCount = parseInt(container.dataset.adyPepperCount);
		if (0 < currentCount) {
			jobsCount += currentCount;
			projectsCount++;
		}
	}
	let infoText = infoMessage.dataset.adyPepperText.replace('{count_projects}', projectsCount.toString());
	infoText = infoText.replace('{count_jobs}', jobsCount.toString());
	infoText = infoText.replace('{total_projects}', totalProjects.toString());
	let percent = Math.floor((projectsCount * 100) / totalProjects).toString();
	infoText = infoText.replace('{percent_projects}', percent);
	infoMessage.textContent = infoText;

	const removalButtons = projectsContainer.querySelectorAll('button.remove');
	for (const button of removalButtons) {
		button.addEventListener('click', removeProject)
	}

	const editButtons = projectsContainer.querySelectorAll('button.edit');
	for (const button of editButtons) {
		button.addEventListener('click', edit)
	}

	const openButtons = projectsContainer.querySelectorAll('.cell.name .open-project-mobile');
	for (const button of openButtons) {
		button.addEventListener('click', openProject)
	}

	usersSearch('translators-autocomplete');
	usersSearch('first-checkers-autocomplete');
	usersSearch('editors-autocomplete');
	usersSearch('last-checkers-autocomplete');

	function edit(e) {
		const id = parseInt(e.currentTarget.dataset.adyPepperId);
		if (!fieldsContainer.classList.contains('closed')) {
			toggleForm();
		}
		const project = document.getElementById(id);
		form.querySelector('input[name="name"]').value = project.querySelector('.cell.name a').innerText;
		form.querySelector('input[name="url"]').value = project.querySelector('.cell.name a').href;
		form.querySelector('input[name="id"]').value = id;
		fillUsersChip(project, 'translators');
		fillUsersChip(project, 'first-checkers');
		fillUsersChip(project, 'editors');
		fillUsersChip(project, 'last-checkers');
		toggleForm();
	}

	function toggleForm() {
		const icon = formTitle.querySelector('i');
		let anchor = projectsContainer;
		if (fieldsContainer.classList.contains('closed')) {
			icon.classList.remove('fa-arrow-circle-down');
			icon.classList.add('fa-arrow-circle-up');
			fieldsContainer.classList.remove('closed');
			anchor = form;
		} else {
			icon.classList.remove('fa-arrow-circle-up');
			icon.classList.add('fa-arrow-circle-down');
			fieldsContainer.classList.add('closed');
			resetForm();
		}
		anchor.scrollIntoView({block: "start", inline: "nearest", behavior: 'smooth'});
	}

	function fillUsersChip(project, job) {
		const users = project.querySelectorAll('.cell.' + job + ' .user.chip');
		if (0 < users.length) {
			for (const user of users) {
				const node = user.cloneNode(true);
				node.title = i18n.chipTitle;
				node.addEventListener('click', removeChip);
				form.querySelector('#' + job + '-autocomplete .show-results').append(node);
			}
		}
	}

	function removeChip(e) {
		e.currentTarget.remove();
	}

	function usersSearch(fieldId) {
		const container = document.querySelector('#' + fieldId + ' .autocomplete-field');
		const searchInput = container.querySelector('input.autocomplete');

		searchInput.addEventListener('input', function(e) {
			const searchData = searchInput.value;

			let choice;
			while (choice = container.getElementsByClassName('autocomplete-result')[0]) {
				choice.remove();
			}
			if (searchData.length >= 3) {
				const formData = new FormData();
				formData.append('q', searchData);
				sendForm(formData, searchEndpoint, apiTokens.search_token, 200, function(request) {
					const data = JSON.parse(request.responseText);
					const wrapper = document.createElement('div');
					wrapper.className = "autocomplete-result";
					container.appendChild(wrapper);
					Object.keys(data).map(function(key, i) {
						const user = data[i];

						const searchResultsContainer = document.createElement('div');
						searchResultsContainer.setAttribute('class', 'row');
						searchResultsContainer.dataset.user = JSON.stringify(user);
						searchResultsContainer.addEventListener('click', chooseUser);

						const username = document.createElement('span');
						username.textContent = user.username;
						const avatar = document.createElement('span');
						avatar.innerHTML = user.avatar;

						wrapper.appendChild(searchResultsContainer);
						searchResultsContainer.appendChild(avatar);
						searchResultsContainer.appendChild(username);
					});

					function closeAutocomplete(e) {
						if (!wrapper.contains(e.currentTarget)) {
							wrapper.remove();
						}
					}

					document.addEventListener('click', closeAutocomplete)
				});
			}
		})
	}

	function chooseUser(e) {
		const user = JSON.parse(e.currentTarget.dataset.user);
		const container = e.currentTarget.parentNode.parentNode.parentNode;
		const showContainer = container.querySelector('div.show-results');
		const autocomplete = container.querySelector('.autocomplete');
		autocomplete.value = '';
		showContainer.type = 'text';
		const chip = document.createElement('div');
		chip.classList.add('user', 'chip');
		chip.dataset.userId = user.id;
		chip.innerHTML = user.avatar;
		chip.title = 'Cliquez pour supprimer';
		chip.addEventListener('click', removeChip);
		const username = document.createElement('span');
		username.textContent = user.username;
		chip.append(username);
		chip.dataset.value = user.id;
		showContainer.append(chip);
	}

	function submitForm(e) {
		e.preventDefault();
		const title = form.querySelector('input[name="name"]').value;
		const url = form.querySelector('input[name="url"]').value;
		const id = form.querySelector('input[name="id"]').value;

		const formData = new FormData();

		if (id) {
			formData.append('id', id);
		}
		formData.append('title', title);
		formData.append('url', url);
		appendUsersIdToForm('translators', formData, 'translators');
		appendUsersIdToForm('first-checkers', formData, 'first_checkers');
		appendUsersIdToForm('editors', formData, 'editors');
		appendUsersIdToForm('last-checkers', formData, 'last_checkers');

		sendForm(formData, createEndpoint, apiTokens.create_token, 202, function() {
			location.reload();
		});
	}

	function appendUsersIdToForm(job, formData, key) {
		const chips = form.querySelectorAll('#' + job + '-autocomplete .show-results .user.chip');
		let usersId = null;
		for (const chip of chips) {
			if (null !== usersId) {
				usersId += ',' + chip.dataset.userId;
				continue;
			}
			usersId = chip.dataset.userId;
		}

		if (null !== usersId) {
			formData.append(key, usersId);
		}
	}

	function resetForm() {
		form.querySelector('input[name="name"]').value = '';
		form.querySelector('input[name="url"]').value = '';
		form.querySelector('input[name="id"]').value = '';
		const jobs = form.querySelectorAll('.show-results');
		for (const job of jobs) {
			job.textContent = '';
		}
	}

	function removeProject(e) {
		const name = e.currentTarget.dataset.adyPepperName;
		const id = parseInt(e.currentTarget.dataset.adyPepperId);
		const question = i18n.removalConfirmation.replace('{title}', name);
		if (!confirm(question)) {
			return;
		}

		const formData = new FormData();
		sendForm(formData, deleteEndpoint + '/' + id, apiTokens.delete_token, 202, function() {
			location.reload();
		});
	}

	function openProject(e) {
		const id = parseInt(e.currentTarget.dataset.adyPepperId);
		const project = document.getElementById(id);
		const secondaryCells = project.querySelector('.secondary-cells');
		secondaryCells.style.display = secondaryCells.style.display === 'flex' ? 'none' : 'flex';
	}

	function sendForm(formData, endpoint, token, statusCode, callback) {
		const request = new XMLHttpRequest();
		formData.append('creation_time', apiTokens.creation_time);
		formData.append('form_token', token);
		request.open('POST', endpoint);
		request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

		request.onreadystatechange = function() {
			if (request.readyState === XMLHttpRequest.DONE) {
				if (statusCode === request.status) {
					callback(request)
				} else {
					errorMessage.textContent = i18n.generalError;
				}
			}
		}

		request.send(formData);
	}

	const autocompleteFieldContainers = document.querySelectorAll('.autocomplete-field-container');
	for (const container of autocompleteFieldContainers) {
		container.addEventListener('keydown', function(e) {
			const results = container.querySelector('.autocomplete-result');
			if (!results?.hasChildNodes()) return;
			const selected = results.querySelector('.row[data-selected]');

			if ('Enter' === e.key) {
				e.preventDefault();
				simulateClick(selected);
				return;
			}

			let dir = null;
			if ('ArrowDown' === e.key) dir = true;
			if ('ArrowUp' === e.key) dir = false;
			if (null === dir) return;
			e.preventDefault();

			if (!selected) {
				// select the first element
				const element = (true === dir) ? results.firstElementChild : results.lastElementChild;
				if (element) element.dataset.selected = '';
				return;
			}

			// select the next element
			delete selected.dataset.selected;
			const nextElement = (true === dir) ? selected.nextElementSibling : selected.previousElementSibling;
			if (!nextElement) return;
			nextElement.dataset.selected = '';
		});
	}

	function simulateClick(element) {
		const event = new MouseEvent('click', {
			view: window,
			bubbles: true,
			cancelable: true
		});
		element.dispatchEvent(event);
	}
})();
