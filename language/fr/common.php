<?php

/**
 * @package   phpBB Extension - DC-Trad Pepper
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = [];
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, [
	'TAB_TITLE'            => 'Pepper',
	'PAGE_TITLE'           => 'Hello! I’m Pepper.',
	'TRANSLATORS_TITLE'    => 'Trad',
	'EDITORS_TITLE'        => 'Edit',
	'FIRST_CHECKERS_TITLE' => 'C1',
	'LAST_CHECKERS_TITLE'  => 'C2',
	'TITLE'                => 'Titre',
	'URL'                  => 'URL',
	'ACTIONS'              => 'Actions',
	'XML_REQUEST_ERROR'    => 'Une erreur est survenue. Merci de contacter un administrateur.',
	'REMOVAL_CONFIRMATION' => 'Voulez-vous vraiment supprimer {title} ?',
	'CHIP_TITLE'           => 'Cliquez pour supprimer',
	'INFO_COUNT_PROJECTS'  => 'Mes projets : {count_projects}/{total_projects} soit {percent_projects}% | Mes postes : {count_jobs}',
]);
