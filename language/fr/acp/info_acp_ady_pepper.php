<?php

/**
 * @package   phpBB Extension - DC-Trad Pepper
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, [
	'ACP_CAT_DCT'                     => 'DC-Trad',
	'LOG_PEPPER_FETCH_PROJECTS_ERROR' => '<strong>[Pepper] Impossible de récupérer les infos des projets</strong><br>» Exception : %1$s',
	'LOG_PEPPER_FETCH_USERS_ERROR'    => '<strong>[Pepper] Impossible de récupérer les infos des utilisateurs</strong><br>» Requête : %1$s<br>» Exception : %2$s',
	'LOG_PEPPER_UPSERT_PROJECT'       => '<strong>[Pepper] Un projet va être créé ou modifié</strong><br>» Paramètres : %1$s',
	'LOG_PEPPER_UPSERT_PROJECT_ERROR' => '<strong>[Pepper] Impossible de créer ou modifier un projet</strong><br>» Paramètres : %1$s<br>» Exception : %2$s',
	'LOG_PEPPER_DELETE_PROJECT'       => '<strong>[Pepper] Un projet va être supprimé</strong><br>» ID : %1$s',
	'LOG_PEPPER_DELETE_PROJECT_ERROR' => '<strong>[Pepper] Impossible de supprimer un projet</strong><br>» ID : %1$s<br>» Exception : %2$s',
]);
