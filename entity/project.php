<?php

namespace ady\pepper\entity;

use ady\pepper\constant\team_status;

class project
{
	public int $id;
	public string $name;
	public \ArrayObject $translators;
	public \ArrayObject $editors;
	public \ArrayObject $first_checkers;
	public \ArrayObject $last_checkers;
	public string $url;
	public int $status;
	public int $count;

	public function __construct()
	{
		$this->translators = new \ArrayObject();
		$this->editors = new \ArrayObject();
		$this->first_checkers = new \ArrayObject();
		$this->last_checkers = new \ArrayObject();
	}

	public function get_id(): int
	{
		return $this->id;
	}

	public function set_id(int $id): project
	{
		$this->id = $id;

		return $this;
	}

	public function get_name(): string
	{
		return $this->name;
	}

	public function set_name(string $name): project
	{
		$this->name = $name;

		return $this;
	}

	public function get_translators(): \ArrayObject
	{
		return $this->translators;
	}

	public function add_translator(array $user): project
	{
		if (!$this->translators->offsetExists($user['user_id']))
		{
			$this->translators->offsetSet($user['user_id'], $user);
		}

		return $this;
	}

	public function remove_translator(array $user): project
	{
		if ($this->translators->offsetExists($user['user_id']))
		{
			$this->translators->offsetUnset($user['user_id']);
		}

		return $this;
	}

	public function get_editors(): \ArrayObject
	{
		return $this->editors;
	}

	public function add_editor(array $user): project
	{
		if (!$this->editors->offsetExists($user['user_id']))
		{
			$this->editors->offsetSet($user['user_id'], $user);
		}

		return $this;
	}

	public function remove_editor(array $user): project
	{
		if ($this->editors->offsetExists($user['user_id']))
		{
			$this->editors->offsetUnset($user['user_id']);
		}

		return $this;
	}

	public function get_first_checkers(): \ArrayObject
	{
		return $this->first_checkers;
	}

	public function add_first_checker(array $user): project
	{
		if (!$this->first_checkers->offsetExists($user['user_id']))
		{
			$this->first_checkers->offsetSet($user['user_id'], $user);
		}

		return $this;
	}

	public function remove_first_checker(array $user): project
	{
		if ($this->first_checkers->offsetExists($user['user_id']))
		{
			$this->first_checkers->offsetUnset($user['user_id']);
		}

		return $this;
	}

	public function get_last_checkers(): \ArrayObject
	{
		return $this->last_checkers;
	}

	public function add_last_checker(array $user): project
	{
		if (!$this->last_checkers->offsetExists($user['user_id']))
		{
			$this->last_checkers->offsetSet($user['user_id'], $user);
		}

		return $this;
	}

	public function remove_last_checker(array $user): project
	{
		if ($this->last_checkers->offsetExists($user['user_id']))
		{
			$this->last_checkers->offsetUnset($user['user_id']);
		}

		return $this;
	}

	public function get_url(): string
	{
		return $this->url;
	}

	public function set_url(string $url): project
	{
		$this->url = $url;

		return $this;
	}

	public function set_status(): project
	{
		$this->status = team_status::EMPTY;
		$total = $this->translators->count() + $this->editors->count() + $this->first_checkers->count() + $this->last_checkers->count();

		if (3 === $total)
		{
			$this->status = team_status::MISSING;

			return $this;
		}

		if (4 <= $total)
		{
			$this->status = team_status::FULL;
		}

		return $this;
	}

	public function set_count(int $count): project
	{
		$this->count = $count;

		return $this;
	}
}
